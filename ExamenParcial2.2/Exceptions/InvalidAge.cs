﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenParcial2._2.Exceptions
{
    class InvalidAge:SystemException
    {
        public InvalidAge() { }

        public InvalidAge(string Message) : base(Message) { }

        public InvalidAge(string Message, Exception innerE) : base(Message, innerE) { }
        
    }
}
