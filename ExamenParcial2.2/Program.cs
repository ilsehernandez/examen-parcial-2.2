﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExamenParcial2._2.Exceptions;

namespace ExamenParcial2._2
{
    class Program
    {
        static void Main(string[] args)
        {

            string name;
            int age;

            try
            {
                Console.WriteLine("Name: ");
                name = Console.ReadLine();
                Console.WriteLine("Age: ");
                age = int.Parse(Console.ReadLine());
                if (age < 0)
                {
                    throw new InvalidAge("Edad no valida");
                }
            }
            catch(InvalidAge ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            
        }
    }
}
